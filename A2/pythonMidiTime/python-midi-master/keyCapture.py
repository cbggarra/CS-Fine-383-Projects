from pynput import keyboard
import midi
import time as time_
import threading


# https://stackoverflow.com/questions/29604205/make-python-script-exit-after-x-seconds-of-inactivity
class Watchdog():
    def __init__(self, timeout):
        self.timeout = timeout
        self._t = None

    def do_expire(self):
        write_and_refresh()

    def _expire(self):
        print("\nWatchdog expire")
        self.do_expire()
        self._t = None

    def start(self):
        if self._t is None:
            self._t = threading.Timer(self.timeout, self._expire)
            self._t.start()
        else:
            self.refresh()

    def stop(self):
        if self._t is not None:
            self._t.cancel()
            self._t = None

    def refresh(self):
        if self._t is not None:
             self.stop()
             self.start()

# create a watchdog with 2second timeout
wd = Watchdog(2)
# Instantiate a MIDI Pattern (contains a list of tracks)
pattern = midi.Pattern()
# Instantiate a MIDI Track (contains a list of MIDI events)
track = midi.Track()
# Append the track to the pattern
pattern.append(track)

# keey track of the time of the last midi event recorded
lastMidiEventTime = int(round(time_.time() * 1000))
# keep track of file number (for naming)
fileNum = 0

# key down callback
def on_press(key):
    global lastMidiEventTime
    global wd
    # start the watchdog (also acts as a refresh)
    wd.start()
    #get time of this midi event
    evTime = int(round(time_.time() * 1000))
    # trims black space at start of track
    if lastMidiEventTime == -1:
        lastMidiEventTime = evTime
    try:
        print('alphanumeric key {0} pressed, tick {1}'.format(
            key.char, evTime - lastMidiEventTime))
        # Instantiate a MIDI note on event, append it to the track, lower pitch by 30 because it sounds better
        on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=ord(key.char)-30)
        track.append(on)
    #attributError means key doesnt have key.char instantiated, aka is a special key
    except AttributeError:
        print('special key {0} pressed, tick {1}'.format(
            key, evTime - lastMidiEventTime))
        # Instantiate a MIDI note on event, append it to the track
        # special mapping for the limited set of special chars on the truncated keyboard
        if key == key.backspace:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.A_3)
        elif key == key.space:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.B_3)
        elif key == key.shift:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.C_3)
        elif key == key.caps_lock:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.D_3)
        elif key == key.enter:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.E_3)
        elif key == key.enter:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.F_3)
        else:
            on = midi.NoteOnEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.G_3)
        track.append(on)
    lastMidiEventTime = int(round(time_.time() * 1000))

# key up callback
def on_release(key):
    global lastMidiEventTime
    global wd
    # start the watchdog (also acts as a refresh)
    wd.start()
    #get time of this midi event
    evTime = int(round(time_.time() * 1000))
    try:
        print('{0} released, tick {1}'.format(key,evTime - lastMidiEventTime))
        # Instantiate a MIDI note off event, append it to the track, lower pitch by 30 because it sounds better
        off = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), pitch=ord(key.char)-30)
        track.append(off)
    except AttributeError:
        print('special key {0} pressed, tick {1}'.format(
            key, evTime - lastMidiEventTime))
        # Instantiate a MIDI note on event, append it to the track
        # important that the mapping for special keys is the same or the notes may never end
        if key == key.backspace:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.A_3)
        elif key == key.space:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.B_3)
        elif key == key.shift:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.C_3)
        elif key == key.caps_lock:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.D_3)
        elif key == key.enter:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.E_3)
        elif key == key.enter:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.F_3)
        else:
            on = midi.NoteOffEvent(tick=(evTime - lastMidiEventTime), velocity=120, pitch=midi.G_3)
        track.append(on)
        if key == keyboard.Key.esc:
            return False
    lastMidiEventTime = int(round(time_.time() * 1000))

# write a midi file and setup variables for next midi file to be created
def write_and_refresh():
    global pattern
    global track
    global fileNum
    global lastMidiEventTime
    print 'File saved: {0}'.format("keyboardMidi-" + str(fileNum) + ".mid")
    # Stop listener
    # Save the pattern to disk
    midi.write_midifile("../randomMidi/data/keyboardMidi-" + str(fileNum) + ".mid", pattern)
    fileNum += 1
    # stop the watchdog and set lastMidiEventTime to -1 to show end of track
    wd.stop()
    lastMidiEventTime = -1
    #reset variables
    # Instantiate a MIDI Pattern (contains a list of tracks)
    pattern = midi.Pattern()
    # Instantiate a MIDI Track (contains a list of MIDI events)
    track = midi.Track()
    # Append the track to the pattern
    pattern.append(track)

# Collect events until released
with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()
