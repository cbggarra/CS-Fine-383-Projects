import java.util.Collection;
import javax.sound.midi.*;

AMidiPlayer midiPlayer;
PShader shader;

void setup() {
  size(600, 600, P3D);
  background(0);
  colorMode(HSB);
  noStroke();
  midiPlayer = new AMidiPlayer();  
  midiPlayer.load(dataPath(getRandomDataFile()));
  midiPlayer.start(); 
}

void draw() {
  if(midiPlayer.isPlaying() == false) {
      midiPlayer = new AMidiPlayer();
      midiPlayer.load(dataPath(getRandomDataFile()));
      midiPlayer.start();
  }
  midiPlayer.update();
}

String getRandomDataFile() {
  //update the data path
  String path = dataPath(""); 
  File dataFolder = new File(path); 
  String[] fileList = dataFolder.list();
  if(fileList.length == 0) {
    return ""; 
  }
  int fileNum = int(random(0,fileList.length));
  return fileList[fileNum];
}
