import generativedesign.*;
import processing.opengl.*;

//wrapper class to help keep track of node/attractor pairs
class togglePositionNode {
  //the node itself
  Node node;
  //springs used soley to activate propogation events
  //DO NOT UPDATE SPRINGS HERE
  parametricSpring[] parametricSprings;
  Attractor attractorPosn;
  float killProbability = -1;
  int bumpMag = 5;
  nodeMesh parent;
  
  // create agent that picks starting position itself
  //togglePositionNode(Node newNode,ArrayList<Spring> newSprings,Attractor newAttractorPosn) {
  togglePositionNode(Node newNode,Attractor newAttractorPosn,nodeMesh newParent) {
    parent = newParent;
    
    node = newNode;
    
    parametricSprings = new parametricSpring[0];

    attractorPosn = newAttractorPosn;
  }
  
  //add a spring that this node is connected to
  void addSpring(parametricSpring newSpring) {
      parametricSprings = (parametricSpring[]) append(parametricSprings, newSpring);
  }

  void update(Boolean usePosn) {
    if(random(0,100) < killProbability) {
      parent.dataKilled();
      println("killed");
      return; 
    }
    // attract
    if(usePosn) {
      attractorPosn.attract(node);
    }
    // update node positions
    node.update();
  }
   
  
  void draw() {  
    // draw attractor
    //stroke(0, 50);
    //strokeWeight(1);
    //noFill();
    //line(attractorPosn.x-10, attractorPosn.y, attractorPosn.x+10, attractorPosn.y);
    //line(attractorPosn.x, attractorPosn.y-10, attractorPosn.x, attractorPosn.y+10);
    //ellipse(attractorPosn.x, attractorPosn.y, attractorPosn.radius*2, attractorPosn.radius*2);

    //// draw node
    //noStroke();
    //fill(0);

    //pushMatrix();
    //translate(node.x, node.y, node.z);
    //sphere(1);
    //popMatrix();

  }
  
  //data has arrived from a spring to this node
  void getData(PVector d,int faceNum) {
    if(random(0,100) < killProbability) {
      parent.dataKilled();
      println("killed");
      return; 
    }
    d.setMag(bumpMag + bumpMag*faceNum);
    node.x += d.x;
    node.y += d.y;
    node.z += d.z;
    int springNum = int(random(0,parametricSprings.length));
    // some nodes may get data before springs are instantiated
    if(springNum < parametricSprings.length) {
      parametricSprings[springNum].sendData(this);
    }
  }
}
