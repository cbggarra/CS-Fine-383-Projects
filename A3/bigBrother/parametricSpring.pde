import generativedesign.*;
import processing.opengl.*;

//wrapper class to help keep track of node/attractor pairs
class parametricSpring {
  //the nodes the spring attaches to
  togglePositionNode nodeA, nodeB;
  // parametric value of 'data' location
  float t = 0;
  float step = 2;
  Boolean hasData = false;
  //the nodes the spring attaches to
  togglePositionNode dataStart, dataEnd;
  //a vector for the arc from start to end
  PVector dataVector;
    nodeMesh parent;

  

  

  // create agent that picks starting position itself
  //togglePositionNode(Node newNode,ArrayList<Spring> newSprings,Attractor newAttractorPosn) {
  parametricSpring(togglePositionNode newNodeA,togglePositionNode newNodeB, nodeMesh newParent) {
    parent = newParent;
    nodeA = newNodeA;
    nodeB = newNodeB;
  }

  void update(Boolean usePosn, int faceNum) {
    if(hasData) {
      dataVector = new PVector(dataEnd.node.x-dataStart.node.x,dataEnd.node.y-dataStart.node.y,dataEnd.node.z-dataStart.node.z);
      t += step + (faceNum*step);
      if(t > dataVector.mag()) {
        t = 0; 
        hasData = false;
        dataEnd.getData(dataVector,faceNum);
      }
    }
  }
   
  
  void draw() {  
     // draw spring
  
  //stroke(0, 134, 153);
  stroke(0,255,0);
  
  strokeWeight(1.5);
  line(nodeA.node.x, nodeA.node.y, nodeA.node.z, nodeB.node.x, nodeB.node.y, nodeB.node.z);
    if(hasData) {
      PVector drawVector = dataVector;
      drawVector.setMag(t);
      pushMatrix();
      translate(dataStart.node.x+drawVector.x,dataStart.node.y+drawVector.y,dataStart.node.z+drawVector.z);
      //stroke(250, 120, 0);
      stroke(81, 255, 45);
      sphere(2);
      popMatrix();
    }
  }
  
  void sendData(togglePositionNode start) {
    if(hasData) {
      parent.dataKilled();
      // kill the later data if it collides
      return; 
    }
    dataStart = start;
    if(start == nodeA) {
      dataEnd = nodeB;  
    } else {
      dataEnd = nodeA;  
    }
    dataVector = new PVector(dataEnd.node.x-dataStart.node.x,dataEnd.node.y-dataStart.node.y,dataEnd.node.z-dataStart.node.z);

    hasData = true;
  }
}
