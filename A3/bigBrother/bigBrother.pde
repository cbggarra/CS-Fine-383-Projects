/**
 * WhichFace
 * Daniel Shiffman
 * http://shiffman.net/2011/04/26/opencv-matching-faces-over-time/
 *
 * Modified by Jordi Tost (@jorditost) to work with the OpenCV library by Greg Borenstein:
 * https://github.com/atduskgreg/opencv-processing
 *
 * @url: https://github.com/jorditost/BlobPersistence/
 *
 * University of Applied Sciences Potsdam, 2014
 */

import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import de.looksgood.ani.*;

Capture video;
OpenCV opencv;

// List of my Face objects (persistent)
ArrayList<Face> faceList;

// List of detected faces (every frame)
Rectangle[] faces;

// Number of faces detected over all time. Used to set IDs.
int faceCount = 0;

// Scaling down the video
int scl = 2;

boolean debug = false;

Face currentTracking = null;


/**
 * part of the example files of the generativedesign library.
 *
 * shows how to use the classes Node, Spring and Attractor in 3d space.
 */
 
import generativedesign.*;
import processing.opengl.*;
nodeMesh mesh;
Gui gui;

Boolean usePosn = true;
//x and y angles for ani to handle
float xAngle = 0;
float yAngle = 0;

//Gui variables
int faceFrameNearSize = 80;
int faceFrameFarSize = 10;
int minZDepth = 3500;
int maxZDepth = 10000;
float initLookDownDeg = 10;


int xOffSet = 0;
int yOffSet = 0;
//spawn and kill rates are in datas/second

void setup() {
  noCursor();
  
  size(1280,720, OPENGL);
  //fullScreen(OPENGL);
  lights();
   String[] cameras = Capture.list();
   println(cameras);
   println(width);
   println(height);
  //video = new Capture(this, 1280/scl, 720/scl, "Microsoft LifeCam HD-3000", 30);
  // this is to allow a different camera size than screen size and still center the cameras image
  xOffSet = (width - 1280)/2;
  yOffSet = (height - 720)/2;
  println(xOffSet);
   println(yOffSet);
  video = new Capture(this, width/scl, height/scl);
  opencv = new OpenCV(this, 1280/scl, 720/scl);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  
  faceList = new ArrayList<Face>();
  
  video.start();
 

  // setup the simple Gui
  gui = new Gui(this);
  mesh = new nodeMesh("headEyes.obj",50);
  fill(0);
  
  sphereDetail(10);
  gui.addSlider("dataAllowed",0,100);
  gui.addSlider("killProbability",0,100);

  gui.addSlider("faceFrameNearSize",0,1000);
  gui.addSlider("faceFrameFarSize",0,100);
  gui.addSlider("minZDepth",0,10000);
  gui.addSlider("maxZDepth",0,100000);
  gui.addSlider("initLookDownDeg",0,360);
  Ani.init(this);
}


void draw() {
  opencv.loadImage(video);

  //image(video, 0, 0 );
  detectFaces();
  
  
  background(0);
  
  if(faceList.size() == 0) {
    usePosn = false;
  } else {
    usePosn = true;
    lookAt(faceList.get(0).r.x*scl + faceList.get(0).r.width*scl/2 + xOffSet,faceList.get(0).r.y*scl + faceList.get(0).r.height*scl/2 + yOffSet,faceList.get(0).r.width*scl);  
  }
  mesh.update(usePosn,faceList.size());
  pushMatrix();
  translate(width/2,height/2,0);
   rotateY(yAngle);
   rotateX(xAngle - radians(initLookDownDeg));
   
  translate(-width/2,-height/2,0);
  mesh.draw();
  popMatrix();
  
  if(debug) {
  image(video, 0, 0 );
      noFill();
    strokeWeight(5);
    stroke(255,255,0);
    if(faceList.size() !=0) {
      
    rect(faceList.get(0).r.x,faceList.get(0).r.y,faceList.get(0).r.width,faceList.get(0).r.height);
    }
    //rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
     //Draw all the faces
     stroke(255,0,0);
    for (Face f : faceList) {
      strokeWeight(2);
      f.display();
    }
  }
}

//tells ani to start updating the globals to look at a new face frame
void lookAt(int x,int y,float frameSize) {
  float virtualZ = map(frameSize,faceFrameNearSize,faceFrameFarSize,minZDepth,maxZDepth);
 PVector vectorToFace = new PVector(x-width/2,y-height/2,virtualZ);
 PVector xzProjection = new PVector(x-width/2,0,virtualZ);
 PVector yzProjection = new PVector(0,y-height/2,virtualZ);
 vectorToFace.setMag(1);

 xzProjection.setMag(1);
 yzProjection.setMag(1);
 if(debug) {
    println("faceVec"+vectorToFace);
    line(width/2,height/2,0,x,y,virtualZ);
 }
 if(vectorToFace.x < 0) {
   Ani.to(this, 0.5, "yAngle", PVector.angleBetween(vectorToFace,yzProjection));  
 } else {
   Ani.to(this, 0.5, "yAngle", -PVector.angleBetween(vectorToFace,yzProjection));
 }
 
 if(vectorToFace.y < 0) {
   Ani.to(this, 0.5, "xAngle", PVector.angleBetween(vectorToFace,xzProjection));
 } else {
   Ani.to(this, 0.5, "xAngle", -PVector.angleBetween(vectorToFace,xzProjection));
 }
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    usePosn = !usePosn;
  }
  
  if (key == 'r') {
    mesh = new nodeMesh("suzanne.obj",200);
  }
}

// call back from Gui
void dataAllowed(int n) {
  mesh.dataAllowed = n;
}

// call back from Gui
void killProbability(float n) {
  mesh.setKillProbability(n);
}

// call back from Gui
void faceFrameNearSize(int n) {
  faceFrameNearSize = n;
}
// call back from Gui
void faceFrameFarSize(int n) {
  faceFrameFarSize = n;
}

// call back from Gui
void minZDepth(int n) {
  minZDepth = n;
}

// call back from Gui
void maxZDepth(int n) {
  maxZDepth = n;
}

// call back from Gui
void initLookDownDeg(float n) {
  initLookDownDeg = n;
}
