import generativedesign.*;
import processing.opengl.*;

class nodeMesh {
  togglePositionNode[] togglePositionNodes;
  Node[] nodes;
  parametricSpring[] parametricSprings;
  Attractor attractorMid;
  float ossocelationRate=1;
  float ossocelationMag = 0;
  int dataAllowed = 0;
  int currentData = 0;
  int timer = millis();
  int elapseTime = 30000;
  

  // create agent that picks starting position itself
  nodeMesh(String objectName,int scaleFactor) {
    togglePositionNodes = new togglePositionNode[0];
    nodes = new Node[0];
    parametricSprings = new parametricSpring[0];

    
    String[] input = loadStrings(objectName);
    int lineNum = 0;
    String currLine = input[lineNum];
    String[] chunks = split(currLine, " ");
    
    //v 0.679688 0.453125 0.492188
    while(chunks[0].equals("v")) {
      //do stuff
      float w = width/2 + (float(chunks[1])*scaleFactor);
      float h = height/2 - (float(chunks[2])*scaleFactor);
      float d = (float(chunks[3])*scaleFactor);
      Node n = new Node(w, h, d);
      n.setStrength(-2);
      n.setDamping(0.1);
      n.setBoundary(0, 0, -300, width, height, 300);
      Attractor a = new Attractor(w, h, d);
      a.setMode(Attractor.SMOOTH);
      a.setRadius(1000);
      a.setStrength(10);
      togglePositionNodes = (togglePositionNode[]) append(togglePositionNodes,new togglePositionNode(n,a,this));
      nodes = (Node[]) append(nodes,n);
      //update
      lineNum += 1;
      currLine = input[lineNum];
      chunks = split(currLine, " ");
    }
    
    //vn -0.165288 0.609790 0.775109
    //s 1
    while(chunks[0].equals("vn") || chunks[0].equals("s") || chunks[0].equals("usemtl")) {
      //do nothing and update
      currLine = input[lineNum];
      chunks = split(currLine, " ");
      lineNum++;
    }
    
    // used to update average
    int numArcs = 0;
    //f 32//36 34//37 30//33
    //note vertex num index starts at 1
    //f vertex//normal vertex//normal vertex//normal             
    while(chunks[0].equals("f") && lineNum < input.length) {

      int vertA = int(split(chunks[1],"//")[0]) -1;
      int vertB = int(split(chunks[2],"//")[0]) -1;
      int vertC = int(split(chunks[3],"//")[0]) -1;
      //arc 1
      
      parametricSpring newParametricSpringA = new parametricSpring(togglePositionNodes[vertA], togglePositionNodes[vertB],this);
      togglePositionNodes[vertA].addSpring(newParametricSpringA);
      togglePositionNodes[vertB].addSpring(newParametricSpringA);
      parametricSprings = (parametricSpring[]) append(parametricSprings, newParametricSpringA);
      //arc 2
      parametricSpring newParametricSpringB = new parametricSpring(togglePositionNodes[vertB], togglePositionNodes[vertC],this);
      togglePositionNodes[vertB].addSpring(newParametricSpringB);
      togglePositionNodes[vertC].addSpring(newParametricSpringB);
      parametricSprings = (parametricSpring[]) append(parametricSprings, newParametricSpringB);
      //arc 3
      parametricSpring newParametricSpringC = new parametricSpring(togglePositionNodes[vertC], togglePositionNodes[vertA],this);
      togglePositionNodes[vertC].addSpring(newParametricSpringC);
      togglePositionNodes[vertA].addSpring(newParametricSpringC);
      parametricSprings = (parametricSpring[]) append(parametricSprings, newParametricSpringC);
      
      // update
      lineNum++;
      if(lineNum == input.length) {
        continue; 
      }
      currLine = input[lineNum];
      chunks = split(currLine, " ");
    }

  attractorMid = new Attractor(width/2, height/2, 0);
  attractorMid.setMode(Attractor.SMOOTH);
  attractorMid.setRadius(1000);
  attractorMid.setStrength(25);
 
  }
  
  synchronized void dataKilled() {
    if(currentData > 0) {
      --currentData;
    }
  }

  void update(Boolean usePosn,int faceNum) {
    if(millis()-timer >= elapseTime) {
      if(dataAllowed > 0) {
        dataAllowed -= 0;
      }
      timer = millis();
    }
    if(!usePosn) {
      // update posnNodes
        // let all nodes repel each other
      for (int i = 0 ; i < nodes.length; i++) {
        nodes[i].attract(nodes);
      } 
      
      attractorMid.attract(nodes);
    }
    
    // update spring
      for(parametricSpring s: parametricSprings) {
        s.update(usePosn,faceNum);
      }  
    

    // update posnNodes
    for(togglePositionNode n: togglePositionNodes) {
      n.update(usePosn);
    }
  }
  
  float distance(Node a,Node b) {
    PVector vector = new PVector(a.x-b.x,a.y-b.y,a.z-b.z);
    //println(vector.mag());
    return vector.mag();
  }
  
  void setKillProbability(float killProbability) {
   for(int i=0;i<togglePositionNodes.length;++i) {
     togglePositionNodes[i].killProbability = killProbability;  
   }
  }
  
  void addNewFace() {
   dataAllowed +=1; 
   timer = millis();
  }
   
  
  void draw() {  
    // draw attractor
  stroke(0, 50);
  strokeWeight(1);
  noFill();
  //line(attractorMid.x-10, attractorMid.y, attractorMid.x+10, attractorMid.y);
  //line(attractorMid.x, attractorMid.y-10, attractorMid.x, attractorMid.y+10);
  //ellipse(attractorMid.x, attractorMid.y, attractorMid.radius*2, attractorMid.radius*2);

  // draw spring
    for(parametricSpring s: parametricSprings) {
      s.draw();
    }
  // draw nodes
    for(togglePositionNode n: togglePositionNodes) {
      n.draw();
    }
    //println("allowed" + (dataAllowed + (ossocelationMag * sin(frameCount/ossocelationRate))));
    //println("current" + currentData);
    if(currentData < (dataAllowed + (ossocelationMag * sin(frameCount/ossocelationRate)))) {
      int index = int(random(0,togglePositionNodes.length));
      togglePositionNodes[index].getData(new PVector(0,0,0),0);
      ++currentData;
    }

  }
}
