/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

// add your agent parameters here
float param = 1;

void setup() {
  //size(800, 600, P3D);
  fullScreen();

  agentsCount = 2;

  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("param", 0, 5);
  createAgents();
}

void createAgents() {

  background(255);
  agents = new ArrayList<Agent>();
  
  // create Agents in a centred starting grid
  String[] lines = loadStrings("Agent.pde");
  //String[] lines = loadStrings("Latin-Lipsum.txt");
  println("there are " + lines.length + " lines");
  for (int row = 0 ; row < lines.length; row++) {
    println(lines[row]);
    String line = lines[row];
    int lineLength = line.length();
    for (int col = 0; col < lineLength; col++) {
      char myChar = line.charAt(col);
      Agent a = new Agent(row,col,myChar);
      println(myChar);
      agents.add(a); //<>//
    }
  }
}

void draw() {
  background(255);
  //draw all the agents
  for (Agent a : agents) {
    a.draw();
  }
  
   //update all agents 
  for (Agent a : agents) {
    a.update();
  }
  

  // draw Gui last
  //gui.draw();
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();
  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}
