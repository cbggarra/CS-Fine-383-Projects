
class Agent {
  int x,y,r,g,b;
  char myChar;
  int dx = 7;
  int dy = 10;
  
  // create agent that picks starting position itself
  Agent(int row, int col, char newChar) {
    x = col*dy;
    y = row*dx;
    
    myChar = newChar;
    setCharColor();
  }

  void setCharColor() {
    r=255;
    g=255;
    b=255;
    //capitols
    if(myChar > 65 && myChar < 90) {
      r = 0;
      g = 255;
      b = 0;
    }
    //lowercase
    if(myChar > 97 && myChar < 122) {
      r = 50; 
      g = 200;
      b = 100;
    }
    //digits
    if(myChar > 48 && myChar < 57) {
       r = 255;
       g = 0;
       b = 0;
    }
    //operators
    if(myChar == '+' || myChar == '-' || myChar == '*' || myChar == '&' || myChar == '%' || myChar == '/' || myChar == '=' || myChar == '<' || myChar == '>') {
      r = 10; 
      g = 10;
      b = 255;
    }
    //brackets
    if(myChar == '(' || myChar == ')' || myChar == '{' || myChar == '}' || myChar == '[' || myChar == ']') {
      r = 255; 
      g = 0;
      b = 255;
    }
    //seperators
    if(myChar == '.' || myChar == ',' || myChar == ';' || myChar == ':') {
       g = 100;
       r = 100;
       b = 255;
    }
    
    
  }

  void update() {
        y-=1;
  }
  

  void draw() {
    noStroke();
    fill(r,g,b);
    rect(x,y,dx,dy);
  }
}
