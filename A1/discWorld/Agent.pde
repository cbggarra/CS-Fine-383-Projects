
class Agent {
  //own points
  PVector p, pOld;
  
  int updates = 0;
  int layerRate = 30;
  
  float stepSize, angle;
  boolean isOutside = false;


  // create agent that picks starting position itself
  Agent() {
    p = new PVector(random(width), random(height), random(0,-255));
    pOld = new PVector(p.x, p.y, p.z);
    stepSize = 1;
   
  }

  // create agent at specific starting position
  Agent(float _x, float _y, float _z) {
    p.x = _x;
    p.y = _y;
    p.z = _z;
  }

  void update() {
    
    // save last position
    pOld.set(p); 
    
    isOutside = false;
    float noiseScale = 300;
    float noiseStrength = 10; 
    angle = noise(p.x/noiseScale, p.y/noiseScale, p.z/noiseScale) * noiseStrength;

    p.x += cos(angle) * stepSize;
    p.y += sin(angle) * stepSize;
    p.z += cos(angle) * stepSize;
    
    if (p.x < -10) isOutside = true;
    else if (p.x > width + 10) isOutside = true;
    else if (p.y < -10) isOutside = true;
    else if (p.y > height + 10) isOutside = true;

    if (isOutside) {
      p.x = random(width);
      p.y = random(height);
      p.z = random(0,255);
      pOld.set(p);    
    }

    //// pick a new position
    //x = nextX(x);
    //y = nextY(y);
    //z = nextZ(z);
    
  }
  
  //float nextX(float _x) {
  //  _x = x + random(-5,5);
  //  while( _x < 0 || _x > width - 1) {
  //    _x = x + random(-5,5);
  //  }
  //  return _x;
  //}
  
  //float nextY(float _y) {
  //  _y = y + random(-5,5);
  //  while( _y < 0 || _y > height - 1) {
  //    _y = y + random(-5,5);
  //  }
  //  return _y;
  //}
  
  //float nextZ(float _z) {
  //  _z = z + random(-5,5);
  //  while( _z > 0 || _z < -100) {
  //    _z = z + random(-5,5);
  //  }
  //  return _z;
  //}

  void draw() {

    // draw a line between last position
    // and current position
    stroke(1);
    line(pOld.x, pOld.y, pOld.z, pOld.x, p.y, p.z);
    ++updates;
    if(updates%layerRate == 0) {
      noStroke();
      fill(p.z,0,0);
      
      pushMatrix();
      translate(pOld.x,pOld.y,pOld.z);

      PVector v1 = new PVector(p.x-pOld.x,p.y-pOld.y,p.z-pOld.z);
      PVector x = new PVector(1,0,0);
      PVector y = new PVector(0,1,0);
      PVector z = new PVector(0,0,1);
      
      float theta = PVector.angleBetween(v1,x);
      rotateX(theta);
      
      theta = PVector.angleBetween(v1,y);
      rotateY(theta);
      
      theta = PVector.angleBetween(v1,z);
      rotateZ(theta);
      ellipse(0,0,100,100);
      popMatrix();
    }
  }
}
