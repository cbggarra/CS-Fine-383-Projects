import toxi.geom.*;

class Agent {
  float rotationAngle = 0;
  PVector a1,c1,c2,a2,a1Old,c1Old,c2Old,a2Old;
  //Bezier Curve Storage  
  Bezier3D curveOld;
  Bezier3D curve;
  
  //Parametric points
  PVector[] points;
  //current parametric value
  int t;
  //distance between anchor points in bezier curve
  int stepSize;
  //parameters for breaking bezier curve into steps and mesh generation
  int steps;
  
  //how often mesh points are generated and how many there are
  int meshStep,meshCount;
  //mesh color
  float meshR,meshG,meshB;
  
  //points for mesh
  PShape snake;
  // variables to buffer points before insertion to PShape
  PVector[] meshPointsOld;
  PVector[] meshPoints;
  
  //debug flag to draw extra stuff
  boolean debug = false;
  
  //bool to determine if the current curve started outside the volume (prevents bouncing on wall)
  boolean startedOutside = false;
  int depth;

  // create agent that picks starting position itself
  Agent(int offset) {
    //set depth and passed params
    depth = -max(height,width);
    stepSize = 400;
    steps = 160;
    meshStep = 10;
    meshCount = 8;    
    
    //randomly generate anchor points and control points for initial curve
    a2 = new PVector(random(width),random(height),random(depth));
    c1 = new PVector(random(width),random(height),random(depth));
    a1 = new PVector(width/2,height/2,depth/2);
    c2 = new PVector(random(width),random(height),random(depth));
    curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
    
    // these are unused so just instantiate
    a1Old = PVector.random3D();
    c1Old = PVector.random3D();
    c2Old = PVector.random3D();
    a2Old = PVector.random3D();
    curveOld = new Bezier3D(new PVector[]{a1Old,c1Old,c2Old,a2Old},true);
    
    // get points for first curve
    points = curve.points(steps);
    t=0;
    
    // set up mesh points 
    snake = createShape(GROUP);
    PVector v1 = new PVector(points[t+1].x-points[t].x,points[t+1].y-points[t].y,points[t+1].z-points[t].z);
    PVector loc = new PVector(points[t].x,points[t].y,points[t].z);
    PVector[] mp = generateMeshPoints(v1,loc,meshCount);
    meshPointsOld = mp;
    meshPoints = mp;
    meshR = random(0,255);
    meshG = random(0,255);
    meshB = random(0,255);
    updateMesh();
    
    //set t offset
    t = -(offset%steps);
  }

  void update() {
    //acount for offset
    if(t < 0) { //<>//
     t += 1;
     return;
    }
    
    //draw goes form t -> (t+1)
    //we should then check t+1 to see if the next drawn point is outside
    //n = normal of the plane we intersected
    PVector n = isOutsideSphere(points[t+1]);
    
    //if normal is the 0 vector its not outside, didnt start outside, or is at the end
    if(((n.x != 0 || n.y != 0 || n.z != 0) && !startedOutside) || t == steps - 2) {
       updateCurves(n); 
       t=0;
    }
    if(t%meshStep == 0) {
      updateMesh();
    }
  }
  
  
  PVector perturbVector(PVector p1,PVector p2,float deg) {
    Vec3D up = new Vec3D(0,1,0);
    //vector we want to perturb
    Vec3D target = new Vec3D(p2.x-p1.x,p2.y-p1.y,p2.z-p1.z).normalize();
    //axis for first rotation 
    Vec3D axis = up.cross(target);
    // rotate around axis by random deg
    Vec3D perturb = target.getRotatedAroundAxis(axis,random(0,radians(deg)));
    //revolve around origonal target to create a cone of possible outcome vectors
    perturb = perturb.getRotatedAroundAxis(target,random(0,2*PI));
    
    //set scale + convert to PVector
    PVector dir = new PVector(perturb.x,perturb.y,perturb.z).setMag(stepSize); 
    return dir;
  }
   
  void updateCurves(PVector n) {
    //updating at the end of parametric drawing
    //(check the normal vector form isOutside)
    if((n.x == 0 && n.y == 0 && n.z == 0)) {
      // did not start outside the volume
      startedOutside = false;
      //update anchor and control points
      a1Old.set(a1);
      c1Old.set(c1);
      a2Old.set(a2);
      c2Old.set(c2);
    
      //set first anchor and control points for G2 continuity
      a1 = new PVector().set(a2Old);
      c1 = new PVector().set(PVector.add(a1,PVector.sub(a2Old,c2Old)));
    
      //create a direction vector for second anchor point
      PVector dir = perturbVector(new PVector().set(points[steps-2]),new PVector().set(points[steps-1]),50);
      
      //get second anchor point
      a2 = PVector.add(a1,dir);

      //direction for second control point
      dir = perturbVector(new PVector().set(a2),new PVector().set(a1),15).setMag(75);
    
      // get second control point
      c2 = PVector.add(a2,dir);
    
      // set new bezier curve
      curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
      
    } 
    //updateing because the curve went outside the bouning volume
    else {
      startedOutside = true;
      a1Old.set(a1);
      c1Old.set(c1);
      a2Old.set(points[t+1]);
      c2Old.set(c2);
    
      a1 = new PVector().set(a2Old);
      // get conrol point to point back into center
      PVector dir = perturbVector(new PVector().set(a1),new PVector(width/2,height/2,depth/2),25).setMag(stepSize/2);
      
      // get c1
      c1 = PVector.add(a1,dir);
    
      // get second anchor point towards the center
      dir = perturbVector(new PVector().set(a1),new PVector(width/2,height/2,depth/2),50);

      // get a2
      a2 = PVector.add(a1,dir);

      // direction vector for c2
      dir = perturbVector(new PVector().set(a2),new PVector().set(a1),5).setMag(stepSize/2);
      
      //get c2
      c2 = PVector.add(a2,dir);
    
      // pdate bezier curve
      curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
    }

    // update points array
    PVector[] tempPoints = curve.points(steps);
    System.arraycopy(tempPoints,0,points,0,steps);
  }

  //bounding sphere, return normal at aprox intersection
  //if not outside sphere return 0 vector
  PVector isOutsideSphere(PVector p) {
    PVector planeNormal = new PVector(width/2 - p.x,height/2 - p.y, depth/2 - p.z);
    float dist = planeNormal.mag();
    //radius of sphere 1.5 * the min dimension on screen
    //if(dist < (min(height,width)*1.5)) {
      if(dist < (min(height,width)*0.6)) {
     planeNormal = new PVector(0,0,0);
    }
    return planeNormal.normalize();
  }
   //<>//
  // d is the direction vector of the current curve segment
  // loc is the point which the vector origonates
  // n is how many points to generate
  PVector[] generateMeshPoints(PVector d, PVector loc, int n) {
    PVector[] meshPoints = new PVector[n];
    //upvector
    Vec3D up = new Vec3D(0,1,0);
    //switch to Vec3D
    Vec3D target = new Vec3D(d.x,d.y,d.z).normalize();
    //get an axis of rotation
    Vec3D axis = up.cross(target).normalize();
    // how large of a step to take around the circle
    float stepSize = radians(360/(n));
    for(int i=0;i<n;++i) {
      //temp is a direction vector like a spoke on a wheel, magnitude determines how far from the center our point is
      Vec3D temp = axis.getRotatedAroundAxis(target,stepSize*i).normalize().scale(random(5,15));
      //move to location and add to return array
      meshPoints[i] = PVector.add(loc,new PVector(temp.x,temp.y,temp.z));
    }
    return meshPoints;
  }
  
  void updateMesh() {
    //update color
    meshR += random(-50,50);
    meshG += random(-50,50);
    meshB += random(-50,50);
    
    //direction for orientation of mesh points
    PVector v1 = new PVector(points[t+1].x-points[t].x,points[t+1].y-points[t].y,points[t+1].z-points[t].z);
    //where to locate the points
    PVector loc = new PVector(points[t].x,points[t].y,points[t].z);
    //generated points
    PVector[] mp = generateMeshPoints(v1,loc,meshCount);
    //update buffers for mesh points
    meshPointsOld = meshPoints;
    meshPoints = mp;
    
    //syling the mesh
    strokeWeight(1);  
    fill(meshR,meshG,meshB);
    
    //new PShape for each segment
    PShape segment = createShape();
    segment.beginShape(TRIANGLE_STRIP);
    //generate traingle strips form mesh buffer arays
    for(int i=0;i<=meshCount;++i) {
      if(i==meshCount) {
        segment.vertex(meshPoints[0].x,meshPoints[0].y,meshPoints[0].z);
        segment.vertex(meshPointsOld[0].x,meshPointsOld[0].y,meshPointsOld[0].z);
      } else {
        segment.vertex(meshPoints[i].x,meshPoints[i].y,meshPoints[i].z);
        segment.vertex(meshPointsOld[i].x,meshPointsOld[i].y,meshPointsOld[i].z);
      }
    }
    //close strip
    segment.endShape(CLOSE);
    //add segment to snake
    snake.addChild(segment);
  }

  void draw() {  
    //account for offset
    if(t<0) {
      return;
    }
    
    pushMatrix();
    //rotation
    translate(width/2,height/2,depth/2);
    rotateY(rotationAngle);
    translate(-width/2,-height/2,-depth/2);
    
    // draw a line between last position
    // and current position
    strokeWeight(1);
    noFill();
    stroke(0.1,0.1,0.1);
    line(points[t].x,points[t].y,points[t].z,points[t+1].x,points[t+1].y,points[t+1].z);

    // debug used to draw anchor point to control point
    if(debug) {
      stroke(0.1,255,0.1,10);
      line(a1.x,a1.y,a1.z,c1.x,c1.y,c1.z);
      stroke(0.1,0.1,255,10);
      line(a2.x,a2.y,a2.z,c2.x,c2.y,c2.z);
    }

    //draw the snake pshape //<>//
    shape(snake);
    //pop rotation
    popMatrix();
    //increment t
    ++t;   
    //update rotation angle
    rotationAngle += 0.01;
  }
}
