/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

int meshCount;

boolean started = false;

// add your agent parameters here
float param = 1;

void setup() {
  //size(800, 600, P3D);
  frameRate(20);
  fullScreen(P3D);
  
  
  agentsCount = 25;
  
  // setup the simple Gui
  gui = new Gui(this);
  gui.addSlider("agentsCount", 1, 200);
  
  
  //createAgents();
}

void createAgents() {

  background(255);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent(i);
    agents.add(a);
  }
}

void draw() {
  
  
   background(204);
   directionalLight(255, 255, 255, 0, 0, -1);
  if(!started) {
    return;
  }
  
  // update all agents
  
  
  for (Agent a : agents) {
    a.update();
  }
  
  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  if(gui == null) {
    createAgents();
    started = true;
    return;
  }
  started = true;

  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}

// call back from Gui
void meshCount(int n) {
  meshCount = n;
  createAgents();
}
