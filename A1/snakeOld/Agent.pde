import toxi.geom.*;

class Agent {
  //own points
  Vec3D p, pOld;
  
  ArrayList<Vec3D> points;
  ArrayList<Vec3D> pointsOld;
  
  int updates = 0;
  int layerRate = 30;
  
  float stepSize, angle, dAngle,dRange,depth, wallBuffer;
  boolean isOutside = false;


  // create agent that picks starting position itself
  Agent() {
    depth = -255;
    wallBuffer = 10;
    pOld = new Vec3D(random(wallBuffer,width-wallBuffer), random(wallBuffer,height-wallBuffer), random(depth+wallBuffer,-wallBuffer));
        //pOld = new Vec3D(width-50,height/2,depth/2);
    Vec3D randomDir = Vec3D.randomVector();
    p = new Vec3D(pOld.add(randomDir));
    dRange = 0.1;
    dAngle = 0;
    stepSize = 1;
  }

  // create agent at specific starting position
  Agent(float _x, float _y, float _z) {
    p.x = _x;
    p.y = _y;
    p.z = _z;
  }

  void update() {
    
    // save last position    
    isOutside = false;
    
    //Vec3D up = new Vec3D(0,1,0);
    //Vec3D perturb = new Vec3D(0,1,0);
    //perturb = up.getRotatedAroundAxis(new Vec3D(1,0,0),random(0.000001,radians(10)));
    //perturb = perturb.getRotatedAroundAxis(new Vec3D(0,1,0),random(0,2*PI));
    
    //Vec3D target = new Vec3D(pOld.x-p.x,pOld.y-p.y,pOld.z-p.z);
    //Vec3D axis = target.cross(up);
    //float theta = target.dot(up);
    //perturb = perturb.getRotatedAroundAxis(axis,theta);
    //perturb = perturb.normalize().scale(stepSize);
    //Vec3D dir = new Vec3D(perturb);
    
    //https://www.openprocessing.org/sketch/84553/
    float cor = dRange*atan(20*dAngle)/PI;
    float randNum = (random(2)-1)*dRange-cor;  //Random number from (-dRange, dRange)
    dAngle+=randNum;                       //We don't change the angle directly
                                           //but its differential - source of the smoothness! 
    
    angle+=dAngle;                         //new angle is angle+dAngle
  
    pOld.set(p);
    
    Vec3D dir = new Vec3D(sin(random(0.8,1.2)*angle) * stepSize,cos(random(0.9,1.1)*angle) * stepSize,sin(random(0.9,1.1)*angle + PI/2) * stepSize);

    Vec3D tempP = new Vec3D(p); //<>//
    tempP = tempP.add(dir);

    //r=d−2(d dot n̂)n̂
    if (tempP.x < wallBuffer) {
      dir = myReflect(dir.normalize(),new Vec3D(-1,0,0)).scale(stepSize*2);
      angle = acos(dir.normalize().x);
    } else if(tempP.x > width - wallBuffer) {
      dir = myReflect(dir.normalize(),new Vec3D(1,0,0)).scale(stepSize*2);
      angle = acos(dir.normalize().x);
    } else if (tempP.y < wallBuffer) {
      dir = myReflect(dir.normalize(),new Vec3D(0,-1,0)).scale(stepSize*2);
      angle = asin(dir.normalize().y);
    } else if(tempP.y > height - wallBuffer) { 
      dir = myReflect(dir.normalize(),new Vec3D(0,1,0)).scale(stepSize*2);
      angle = asin(dir.normalize().y); //<>//
    } else if (tempP.z > -wallBuffer) {
      dir = myReflect(dir.normalize(),new Vec3D(0,0,-1)).scale(stepSize*2);
      angle = acos(dir.normalize().z);
    } else if (tempP.z < depth+wallBuffer) {
      dir = myReflect(dir.normalize(),new Vec3D(0,0,1)).scale(stepSize*2);
      angle = acos(dir.normalize().z);
    }

    p = p.add(dir);
    //// pick a new position
    //x = nextX(x);
    //y = nextY(y);
    //z = nextZ(z);
    
  }
  
  // d and n should be normalized
  Vec3D myReflect(Vec3D d,Vec3D n) {
    float scaleFactor = 2*d.normalize().dot(n);
    Vec3D subt = n.scale(scaleFactor); 
    return d.sub(subt);
  }
  
  //float nextY(float _y) {
  //  _y = y + random(-5,5);
  //  while( _y < 0 || _y > height - 1) {
  //    _y = y + random(-5,5);
  //  }
  //  return _y;
  //}
  
  //float nextZ(float _z) {
  //  _z = z + random(-5,5);
  //  while( _z > 0 || _z < -wallBuffer0) {
  //    _z = z + random(-5,5);
  //  }
  //  return _z;
  //}

  void draw() {

    // draw a line between last position
    // and current position
    stroke(p.z,0.1,0.1);
    strokeWeight(2);
    line(pOld.x, pOld.y, pOld.z, p.x, p.y, p.z);
    
    //front square
    line(wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,wallBuffer,-wallBuffer);  
    line(wallBuffer,wallBuffer,-wallBuffer,wallBuffer,height-wallBuffer,-wallBuffer);
    line(wallBuffer,height-wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,-wallBuffer);
    line(width-wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,-wallBuffer);
    
    // back square
    line(wallBuffer,wallBuffer,depth+wallBuffer,width-wallBuffer,wallBuffer,depth+wallBuffer);  
    line(wallBuffer,wallBuffer,depth+wallBuffer,wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(wallBuffer,height-wallBuffer,depth+wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,wallBuffer,depth+wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    
    //connectors
    line(wallBuffer,wallBuffer,-wallBuffer,wallBuffer,wallBuffer,depth+wallBuffer);  
    line(wallBuffer,height-wallBuffer,-wallBuffer,wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,height-wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    
    ++updates;
    if(updates%layerRate == 0) {
      noStroke();
      fill(p.z,0,0,10);
      
      pushMatrix();
      translate(pOld.x,pOld.y,pOld.z);

      PVector v1 = new PVector(p.x-pOld.x,p.y-pOld.y,p.z-pOld.z);
      PVector x = new PVector(1,0,0);
      PVector y = new PVector(0,1,0);
      PVector z = new PVector(0,0,1);
      
      float theta = PVector.angleBetween(v1,x);
      rotateX(theta);
      
      theta = PVector.angleBetween(v1,y);
      rotateY(theta);
      
      theta = PVector.angleBetween(v1,z);
      rotateZ(theta);
      ellipse(0,0,100,100);
      popMatrix();
    }
  }
  
  //https://stackoverflow.com/questions/3881780/how-do-i-rotate-vectors-using-matrices-in-processing
  Vec3D[] rotateVerts(Vec3D[] verts, float angle, Vec3D axis){
  Vec3D[] clone = new Vec3D[verts.length];
  for(int i = 0; i<verts.length;i++)
    clone[i] = verts[i].getRotatedAroundAxis(axis,angle);
  return clone;
  }
}
