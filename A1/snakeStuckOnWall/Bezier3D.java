import processing.core.PVector;

public class Bezier3D {

  // Used to hold the control points
  private float[] px;
  private float[] py;
  private float[] pz;

  // Used to hold intermediate values when calculating
  // a point on the curve
  private float[] pxi;
  private float[] pyi;
  private float[] pzi;

  private boolean useZ = false;
  // degree = order + 1  i.e. degree = number of points
  // order = cubic : degree = 4
  private int degree;

  /**
   * Create a Bezier object based on points passed as a 2D
   * array. 
   * 
   * @param app 
   * @param points 2D array[point no.][x/y]
   * @param nbrPoints number of points to use from array
   * @param useZ true 3D Bezier : false = 2D Bezier
   */
  public Bezier3D(float[][] points, int nbrPoints, boolean useZ){
    this.useZ = useZ;
    degree = nbrPoints;
    makeArrays();

    for(int i = 0; i < degree; i++){
      px[i] = points[i][0];
      py[i] = points[i][1];
      if(useZ == true){
        pz[i] = points[i][2]; // ignored if useZ is false
      }
    }
  }

  /**
   * Create a Bezier object based on an array of vectors. <br>
   * The bezier curve will use all of the points in the array. <br>
   * 
   * @param points array of control points
   */
  public Bezier3D(PVector[] points, boolean useZ){
    this.useZ = useZ;
    degree = points.length;
    makeArrays();
    for(int i = 0; i < degree; i++){
      px[i] = points[i].x;
      py[i] = points[i].y;
      if(useZ == true)
        pz[i] = points[i].z;
    }
  }

  /**
   * Used by the ctors
   */
  private void makeArrays(){
    px = new float[degree];
    py = new float[degree];
    pxi = new float[degree];
    pyi = new float[degree];
    if(useZ){
      pz = new float[degree];
      pzi = new float[degree];
    }
  }

  /**
   * Forces the bezier to ignore the z values i.e. makes
   * it become 2D.
   */
  public void force2D(){
    useZ = false;
    pz = null;
    pzi = null;
  }

  /**
   * Calculate the point for a given parametric point 't' on the curve. 
   * @param t
   * @return
   */
  public PVector point(float t){
    float t1 = 1.0f - t;
    System.arraycopy(px, 0, pxi, 0, degree);
    System.arraycopy(py, 0, pyi, 0, degree);
    if(useZ)
      System.arraycopy(pz, 0, pzi, 0, degree);      
    for(int j = degree-1; j > 0; j--){
      for(int i = 0; i < j; i++){
        pxi[i] = t1 * pxi[i] + t* pxi[i+1];
        pyi[i] = t1 * pyi[i] + t* pyi[i+1];
        if(useZ){
          pzi[i] = t1 * pzi[i] + t* pzi[i+1];          
        }
      }
    }
    if(useZ)
      return new PVector(pxi[0], pyi[0], pzi[0]);
    else
      return new PVector(pxi[0], pyi[0]);

  }

  /**
   * Calculate the points along the Bezier curve. <br>
   * @param steps the number of points to calculate
   * @return array of PVector holding points
   */
  public PVector[] points(int steps){
    PVector points[] = new PVector[steps];
    float t = 0.0f;
    float dt = 1.0f/(steps -1);
    for(int i = 0; i < steps; i++){
      points[i] = point(t);
      t += dt;
    }
    return points;
  }

}
