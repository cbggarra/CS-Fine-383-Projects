import toxi.geom.*;

class Agent {
  PVector a1,c1,c2,a2,a1Old,c1Old,c2Old,a2Old;
  //own points  
  Bezier3D curveOld;
  Bezier3D curve;
  
  PVector[] points;
  int t;
  
  int updates = 0;
  int layerRate = 30;
  int steps = 60;
  
  boolean debug = true;
  
  float stepSize, angle, dAngle,dRange,depth, wallBuffer;
  boolean isOutside = false;
  boolean startedOutside = false;


  // create agent that picks starting position itself
  Agent() {
    depth = -255;
    wallBuffer = 10;
    stepSize = 250;
    
    a1 = new PVector(random(width-wallBuffer),random(height-wallBuffer),random(depth+wallBuffer));
    c1 = new PVector(random(width-wallBuffer),random(height-wallBuffer),random(depth+wallBuffer));
    a2 = new PVector(width/2,height/2,depth/2);
    c2 = new PVector(random(width-wallBuffer),random(height-wallBuffer),random(depth+wallBuffer));
    curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
    
    a1Old = PVector.random3D();
    c1Old = PVector.random3D();
    c2Old = PVector.random3D();
    a2Old = PVector.random3D();
    curveOld = new Bezier3D(new PVector[]{a1Old,c1Old,c2Old,a2Old},true);
    
    points = curve.points(steps);
    t = 1;
  }

  void update() {
    PVector n = isOutside(points[t+1]);
    if((n.x != 0 || n.y != 0 || n.z != 0) && !startedOutside) {
       updateCurves(n); 
       t=1;
       return;
    }
    // if its not the last segment update t and bail
    if(t != steps - 2) {
      ++t;
      return;
    }
    
    updateCurves(n);
    
    t = 1;
       
  }
  
  
  PVector perturbVector(PVector p1,PVector p2,float deg) {
       // second pertubation
    Vec3D up = new Vec3D(0,1,0);
    //Vec3D perturb = new Vec3D(0,1,0);
    //perturb = up.getRotatedAroundAxis(new Vec3D(1,0,0),random(0,radians(deg)));
    //perturb = perturb.getRotatedAroundAxis(new Vec3D(0,1,0),random(0,2*PI));
    
    Vec3D target = new Vec3D(p2.x-p1.x,p2.y-p1.y,p2.z-p1.z).normalize(); //<>//
    Vec3D axis = up.cross(target);
    float theta = target.dot(up);
    Vec3D perturb = target.getRotatedAroundAxis(axis,random(0,radians(deg)));
    perturb = perturb.getRotatedAroundAxis(target,random(0,2*PI));
    
    //  PVector target = new PVector(p2.x-p1.x,p2.y-p1.y,p2.z-p1.z).normalize();
    //  PVector v1 = new PVector(perturb.x,perturb.y,perturb.z).normalize();
    //  PVector x = new PVector(1,0,0);
    //  PVector y = new PVector(0,1,0);
    //  PVector z = new PVector(0,0,1);
      
   
    //        float theta = PVector.angleBetween(target,z);
    //perturb = perturb.getRotatedAroundAxis(new Vec3D(0,0,1),-theta);

    //  theta = PVector.angleBetween(target,y);
    //perturb = perturb.getRotatedAroundAxis(new Vec3D(0,1,0),-theta);
      
    //         theta = PVector.angleBetween(target,x);
    //perturb = perturb.getRotatedAroundAxis(new Vec3D(1,0,0),-theta);
   
      
    PVector dir = new PVector(perturb.x,perturb.y,perturb.z).setMag(stepSize); 
    return dir;
  }
   
  void updateCurves(PVector n) {
    if(t == steps - 2) {
      
      startedOutside = false;
      a1Old.set(a1);
      c1Old.set(c1);
      a2Old.set(a2);
      c2Old.set(c2);
    
      a1 = new PVector().set(a2Old);
      c1 = new PVector().set(PVector.add(a1,PVector.sub(a2Old,c2Old)));
    
      PVector dir = perturbVector(new PVector().set(points[steps-2]),new PVector().set(points[steps-1]),50);
    
      a2 = PVector.add(a1,dir);

      dir = perturbVector(new PVector().set(a2),new PVector().set(a1),15).setMag(75); //<>//
    
      c2 = PVector.add(a2,dir);
    
      curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
      
    } else {
      startedOutside = true;
      a1Old.set(a1); //<>//
      c1Old.set(c1);
      a2Old.set(points[t]);
      c2Old.set(c2);
    
      a1 = new PVector().set(a2Old);
      
      PVector dir = perturbVector(new PVector().set(a1),new PVector(width/2,height/2,depth/2),5).setMag(stepSize/2);

      c1 = PVector.add(a1,dir);
    //potential index out of bounds
      dir = perturbVector(new PVector().set(points[t-1]),new PVector().set(points[t]),5);
      dir = myReflect(dir,n);
    
      a2 = PVector.add(a1,dir);
      //a2 = new PVector(width/2,height/2,depth/2);
      if(n.x != 0 || n.y != 0 || n.z != 0) {
        print("FREAk\n");
      }

      dir = perturbVector(new PVector().set(a2),new PVector().set(a1),5).setMag(stepSize/2);
      
      c2 = PVector.add(a2,dir);
    
      curve = new Bezier3D(new PVector[]{a1,c1,c2,a2},true);
    }

    PVector[] tempPoints = curve.points(steps);
    System.arraycopy(tempPoints,0,points,0,steps);
  }
  
  PVector isOutside(PVector p) {
    PVector planeNormal = new PVector(0,0,0);
    PVector tempP = new PVector().set(p);
    Boolean isOutside = false;
    if (tempP.x < wallBuffer) {
      isOutside = true;
      planeNormal.set(1,0,0);
      //dir = myReflect(dir.normalize(),new Vec3D(-1,0,0)).scale(stepSize*2);
      //angle = acos(dir.normalize().x);
    } else if(tempP.x > width - wallBuffer) {
      isOutside= true;
      planeNormal.set(-1,0,0);
      //dir = myReflect(dir.normalize(),new Vec3D(1,0,0)).scale(stepSize*2);
      //angle = acos(dir.normalize().x);
    } else if (tempP.y < wallBuffer) {
      isOutside= true;
      planeNormal.set(0,1,0);
      //dir = myReflect(dir.normalize(),new Vec3D(0,-1,0)).scale(stepSize*2);
      //angle = asin(dir.normalize().y);
    } else if(tempP.y > height - wallBuffer) {
      isOutside= true;
      planeNormal.set(0,-1,0);
      //dir = myReflect(dir.normalize(),new Vec3D(0,1,0)).scale(stepSize*2);
      //angle = asin(dir.normalize().y);
    } else if (tempP.z > -wallBuffer) {
      isOutside= true;
      planeNormal.set(0,0,-1);
      //dir = myReflect(dir.normalize(),new Vec3D(0,0,-1)).scale(stepSize*2);
      //angle = acos(dir.normalize().z);
    } else if (tempP.z < depth+wallBuffer) {
      isOutside= true;
      planeNormal.set(0,0,1);
      //dir = myReflect(dir.normalize(),new Vec3D(0,0,1)).scale(stepSize*2);
      //angle = acos(dir.normalize().z);
    }
    
    return planeNormal;
  }
  
  // d and n should be normalized
  PVector myReflect(PVector d,PVector n) {
    Vec3D dNew = new Vec3D(d.x,d.y,d.z);
    Vec3D nNew = new Vec3D(n.x,n.y,n.z);
    float scaleFactor = 2*dNew.normalize().dot(nNew);
    Vec3D subt = nNew.scale(scaleFactor); 
    subt = dNew.sub(subt);
    return new PVector(subt.x,subt.y,subt.z);
    
  }
  
  //float nextY(float _y) {
  //  _y = y + random(-5,5);
  //  while( _y < 0 || _y > height - 1) {
  //    _y = y + random(-5,5);
  //  }
  //  return _y;
  //}
  
  //float nextZ(float _z) {
  //  _z = z + random(-5,5);
  //  while( _z > 0 || _z < -wallBuffer0) {
  //    _z = z + random(-5,5);
  //  }
  //  return _z;
  //}

  void draw() {
    // draw a line between last position
    // and current position
    strokeWeight(2);
    noFill();
    if(debug) {
      stroke(0.1,255,0.1,10);
      line(a1.x,a1.y,a1.z,c1.x,c1.y,c1.z);
      stroke(0.1,0.1,255,10);
      line(a2.x,a2.y,a2.z,c2.x,c2.y,c2.z);
    }
    stroke(a1.z,0.1,0.1);
    
    //bezier(a1.x,a1.y,a1.z,c1.x,c1.y,c1.z,c2.x,c2.y,c2.z,a2.x,a2.y,a2.z);
    line(points[t-1].x,points[t-1].y,points[t-1].z,points[t].x,points[t].y,points[t].z);
    //front square
    line(wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,wallBuffer,-wallBuffer);  
    line(wallBuffer,wallBuffer,-wallBuffer,wallBuffer,height-wallBuffer,-wallBuffer);
    line(wallBuffer,height-wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,-wallBuffer);
    line(width-wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,-wallBuffer);
    
    // back square
    line(wallBuffer,wallBuffer,depth+wallBuffer,width-wallBuffer,wallBuffer,depth+wallBuffer);  
    line(wallBuffer,wallBuffer,depth+wallBuffer,wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(wallBuffer,height-wallBuffer,depth+wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,wallBuffer,depth+wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    
    //connectors
    line(wallBuffer,wallBuffer,-wallBuffer,wallBuffer,wallBuffer,depth+wallBuffer);  
    line(wallBuffer,height-wallBuffer,-wallBuffer,wallBuffer,height-wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,wallBuffer,-wallBuffer,width-wallBuffer,wallBuffer,depth+wallBuffer);
    line(width-wallBuffer,height-wallBuffer,-wallBuffer,width-wallBuffer,height-wallBuffer,depth+wallBuffer);
    
    if(t-1 == 0 && debug) {
      noStroke();
      fill(255,0,0,10);
      
      pushMatrix();
      translate(points[t].x,points[t].y,points[t].z);
      //translate(width/2,height/2,depth/2);

      PVector v1 = new PVector(points[t-1].x-points[t].x,points[t-1].y-points[t].y,points[t-1].z-points[t].z);
      PVector x = new PVector(1,0,0);
      PVector y = new PVector(0,1,0);
      PVector z = new PVector(0,0,1);
      
      float theta = PVector.angleBetween( v1,x);
      rotateX(theta);
      
      theta = PVector.angleBetween(v1,y);
      rotateY(theta);
      
      theta = PVector.angleBetween(v1,z);
      rotateZ(theta);
      ellipse(0,0,50,50);
      popMatrix();
    }
  }
  
  //https://stackoverflow.com/questions/3881780/how-do-i-rotate-vectors-using-matrices-in-processing
  Vec3D[] rotateVerts(Vec3D[] verts, float angle, Vec3D axis){
  Vec3D[] clone = new Vec3D[verts.length];
  for(int i = 0; i<verts.length;i++)
    clone[i] = verts[i].getRotatedAroundAxis(axis,angle);
  return clone;
  }
}
